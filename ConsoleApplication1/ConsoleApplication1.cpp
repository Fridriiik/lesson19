#include <iostream>


class Animal{
public:
    virtual void Voice() {
        std::cout << "Give me food\n";
    }
};


class Neighbour : public Animal {
public:
    void Voice() override {
        std::cout << "drill makes brrrrrrrrrr brrrrrrrrrrrrr\n";
    }
};

class BatinZhigul : public Animal {
public:
    void Voice() override {
        std::cout << "vi vi vi vi vi pr pr pr vi viv pr pr pr pr vi vi vi\n";
    }
};

class Phoenix : public Animal {
public:
    void Voice() override {
        std::cout << "kar kar motherfucker\n";
    }
};

int main()
{
    Animal* neighbour = new Neighbour;
    Animal* batinZhigul = new BatinZhigul;
    Animal* phoenix = new Phoenix;

    Animal* zoo[3] =  { neighbour, batinZhigul, phoenix};

    for (int i = 0; i < 3; i++) {
        zoo[i]->Voice();
    }
}
